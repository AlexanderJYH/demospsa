//
//  CesadosTableViewCell.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 1/11/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class CesadosTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNombre : UILabel!
    
    var obj : Colegio!{
        didSet{
            self.lblNombre.text! = self.obj.apellido
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
