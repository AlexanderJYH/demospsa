//
//  DesaprobadoTableViewCell.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 1/11/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class DesaprobadoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNombre : UILabel!
    
    var objDesaprobado : Any!{
        didSet{
            self.lblNombre.text! = "Desaprobado"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
