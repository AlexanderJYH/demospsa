//
//  ContactosTableViewCell.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 3/13/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class ContactosTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNombre : UILabel!
    
    var objContactos : Prospecto!{
        didSet{
            self.lblNombre.text! = self.objContactos.appellido
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
