//
//  ReportesViewController.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 1/13/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

struct Tiendas {
    
    var nombre = ""
    var detalle : Detalle!
}

struct Detalle {
    
    var apellidos = ""
    var dni = ""
}

class ReportesViewController: UIViewController {

    @IBOutlet weak var listRecicler : UIPickerView!
    @IBOutlet weak var btnLista : UIButton!
    @IBOutlet weak var viewFondo: UIView!
    @IBOutlet weak var constraintAnimation: NSLayoutConstraint!
    
    var arrayDetalle = [Tiendas(nombre: "Alexander", detalle: Detalle(apellidos: "Ynoñan", dni: "73125325")),
    Tiendas(nombre: "Brenda", detalle: Detalle(apellidos: "Viz", dni: "73125325")),
    Tiendas(nombre: "Ihtam", detalle: Detalle(apellidos: "Solis", dni: "73125325")),
    Tiendas(nombre: "Roxana", detalle: Detalle(apellidos: "Dureand", dni: "73125325")),
    Tiendas(nombre: "JOhel", detalle: Detalle(apellidos: "Lopez", dni: "73125325"))]
    
//    var nombre : [ String] = ["Alexander","Brenda","Ihtam","Roxana","JOhel","Fabricio","Grego","Natalia" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnLista.layer.cornerRadius = 3
        self.btnLista.layer.borderWidth = 0.3
        self.btnLista.layer.shadowOpacity = 0.3
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func btnLoista(_ sender : UIButton!){
        self.viewFondo.alpha = 1
        self.constraintAnimation.constant = 200
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ReportesViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrayDetalle.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.arrayDetalle[row].nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let cell = self.arrayDetalle[row].nombre
        self.btnLista.titleLabel?.text = cell
        
        
        
        self.constraintAnimation.constant = 0
    }
}

//extension ReportesViewController: UITableViewDelegate, UITableViewDataSource{
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.ar
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        <#code#>
//    }
//}
