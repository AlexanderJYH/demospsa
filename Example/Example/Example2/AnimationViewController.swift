//
//  AnimationViewController.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 3/13/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

struct Prueba {
    var id     = 0
    var nombre = ""
    var name1       = [Prueba1]()
}

struct Prueba1 {
    var name1 = ""
}

struct Prueba2 {
    var name2 = ""
}
//mover posisicoines
//extension RangeReplaceableCollection where Indices: Equatable {
//    mutating func rearrange(from: Index, to: Index) {
//        precondition(from != to && indices.contains(from) && indices.contains(to), "invalid indices")
//        insert(remove(at: from), at: to)
//    }
//    
//    mutating func ordenSection(_ array : [Any]) ->[Any] {
//        
//        var arrayFinal0 = array[0]
//        var arrayFinal1 = array[1]
//        var arrayFinal2 = array[2]
//        
//    }
//}
class AnimationViewController: UIViewController {

//    @IBOutlet weak var clvPrueba: UICollectionView!
    @IBOutlet weak var tblPrueba : UITableView!
    @IBOutlet weak var lblNombre : UILabel!
    @IBOutlet weak var selectSexxo : UISegmentedControl!
    
    var array = [Prueba]()
    var objServicioId = 0
    var id1 = 1
    var id0 = 0
    var final = [Prueba]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.array = [Prueba(id: 0,nombre: "Pago por consulta (promedio)", name1: [Prueba1(name1: "Clinica Internacional"), Prueba1(name1: "Clinica El Golf"),Prueba1(name1: "Clinica Delgado")]),Prueba(id: 1,nombre: "Cobertura % (promedio)", name1: [Prueba1(name1: "Ambulatorio"), Prueba1(name1: "Hospitalario"),Prueba1(name1: "Maternidad")]),Prueba(id: 2,nombre: "Cobertura % (promedio)", name1: [Prueba1(name1: "Ambulatorio"), Prueba1(name1: "Hospitalario"),Prueba1(name1: "Maternidad")]),Prueba(id: 3,nombre: "Cobertura % (promedio)", name1: [Prueba1(name1: "Ambulatorio"), Prueba1(name1: "Hospitalario"),Prueba1(name1: "Maternidad")]),Prueba(id: 4,nombre: "Cobertura % (promedio)", name1: [Prueba1(name1: "Ambulatorio"), Prueba1(name1: "Hospitalario"),Prueba1(name1: "Maternidad")]),Prueba(id: 5,nombre: "Cobertura % (promedio)", name1: [Prueba1(name1: "Ambulatorio"), Prueba1(name1: "Hospitalario"),Prueba1(name1: "Maternidad")])]
    
//        self.array.rearrange(from: 0, to: 3)
//        print(self.f)
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var lblAnios: UILabel!
    
    lazy var labelView : UILabel! = {
        var objLabel = UILabel()
        objLabel.textColor = .darkText
        return objLabel
    }()
    
    @IBAction func cargarAction(_ sender : Any){
        
        var action = sender as? UISegmentedControl
        
        if action?.selectedSegmentIndex == 0 {
            print("hombre")
        }
        
        if action?.selectedSegmentIndex == 1 {
            print("Mujer")
        }
    }
    
    @IBAction func btnAnio(_ sender: UISlider) {
        
        self.view.addSubview(self.labelView)
        self.labelView.translatesAutoresizingMaskIntoConstraints = false
        self.labelView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        self.labelView.text = "\(Int(sender.value))"
        self.labelView.textColor = .darkGray
        self.labelView.center = self.setUISliderThumbValueWithLabel(slider: sender)
    }
    
    func setUISliderThumbValueWithLabel(slider: UISlider) -> CGPoint {
        let slidertTrack : CGRect = slider.trackRect(forBounds: slider.bounds)
        let sliderFrm : CGRect = slider .thumbRect(forBounds: slider.bounds, trackRect: slidertTrack, value: slider.value)
        return CGPoint(x: sliderFrm.origin.x + slider.frame.origin.x + 20, y: slider.frame.origin.y - 15)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func selectNombre(_ nombre : String){
        self.lblNombre.text = nombre
    }

}

extension AnimationViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.array.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array[section].name1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnimationViewControllerTableViewCell", for: indexPath) as! AnimationViewControllerTableViewCell
        
        cell.textLabel?.text = self.array[indexPath.section].name1[indexPath.row].name1
//        cell.detailTextLabel?.text = self.array[indexPath.row].name2.first!.name2
        return cell
    }
//
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        let obj = self.array[section].nombre

        return obj
    }


//extension AnimationViewController: UICollectionViewDelegate, UICollectionViewDataSource{
//
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return self.array.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnimationCollectionViewCell", for: indexPath) as! AnimationCollectionViewCell
//        cell.objPrueba = self.array[indexPath.row]
//
//        return cell
//    }

//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let obj = self.array[indexPath.row]
//        self.selectNombre(obj.nombre)
//        let cell = collectionView.cellForItem(at: indexPath) as? STCollectionReusableView
//        cell?.lblNombre.text = obj.nombre
//        let kind: String
//        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: "", withReuseIdentifier: "stCHeadCell", for: indexPath) as? STCollectionReusableView
//
//        headerView?.lblNombre.text = obj.nombre
//    }

//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//        switch kind {
//        case UICollectionView.elementKindSectionHeader:
//            guard
//                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "stCHeadCell", for: indexPath) as? STCollectionReusableView else {
//                    fatalError("Invalid view type")
//            }
//            headerView.lblNombre.text = self.array[indexPath.row].nombre
//            return headerView
//        default:
//            assert(false, "Invalid element type")
//        }
//    }

}
