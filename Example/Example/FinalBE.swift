//
//  FinalBE.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 1/12/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class FinalBE: NSObject {

    var arrayDesaprobado = [DesaprobadoBE]()
    var nombreDesaprobado = ""
    
    class func parse(_ json : CSMJSON) -> FinalBE{
        
        var objFinal = FinalBE()
        
        let desaprobados = json.dictionary["desaprobados"]?.array ?? []
        
        for obj in  desaprobados{
            
            objFinal.arrayDesaprobado.append(DesaprobadoBE.parse(obj))
        }
        
        return objFinal
    }
    
}
