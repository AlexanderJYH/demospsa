//
//  DesaprobadoBE.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 1/12/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class DesaprobadoBE: NSObject {

    var nombre = ""
    var state = 0
    
    class func parse(_ json : CSMJSON) -> DesaprobadoBE{
        
        var objBE = DesaprobadoBE()
        
        objBE.nombre = json.dictionary["nombre"]?.stringValue ?? ""
        objBE.state = json.dictionary["nombre"]?.intValue ?? 0
        
        return objBE
    }
    
}
