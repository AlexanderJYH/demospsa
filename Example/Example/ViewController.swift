//
//  ViewController.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 1/11/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

struct Colegio {
    var prueba = ""
    var state = 0
    var nombre = ""
    var apellido = ""
}

struct DesaprobadoFinal {
    var estado : Bool
    var color = ""
    var searchList_titulo = ""
    var searchList_arrayData = [Any]()
}

struct Contactos {
    var Apellidos = ""
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    
    var arrayColegios = [Colegio(state: 1, nombre: "Alexander", apellido: "Ynoñan"),
                         Colegio(state: 2, nombre: "Rozana", apellido: "Ynoñan"),
                         
                        Colegio(state: 3, nombre: "Brenda", apellido: "Ynoñan"),
                        Colegio(state: 1, nombre: "Alexander", apellido: "Ynoñan"),
                        Colegio(state: 1, nombre: "Alexander", apellido: "Ynoñan"),
                        Colegio(state: 2, nombre: "Roxana", apellido: "Durand"),
                        Colegio(state: 3, nombre: "Brenda", apellido: "Ynoñan")]
    
    var filtro = [Colegio]()
    var filtro2 = [Colegio]()
    var filtro3 = [Colegio]()
    var arrayFinal = [Colegio]()
    
    var array = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p","q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

    var arrayFiltroFinal = [DesaprobadoFinal]()
    var showIndexPath = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
//        tableView.estimatedRowHeight = 300
        
        self.filtro = self.arrayColegios.filter({$0.nombre > $0.nombre})
        self.filtro2 = self.arrayColegios.filter({$0.state == 2})
        self.filtro3 = self.arrayColegios.filter({$0.state == 3})
        
       
        var arrayFiltro = [DesaprobadoFinal]()
        
        arrayFiltro.append(DesaprobadoFinal.init(estado: true ,color: "#46983F" ,searchList_titulo: "Aprobado", searchList_arrayData: self.filtro))
        arrayFiltro.append(DesaprobadoFinal.init(estado: true ,color: "#E02020",searchList_titulo: "Desaprobado", searchList_arrayData: self.filtro2))
        arrayFiltro.append(DesaprobadoFinal.init(estado: true ,color: "#B3B3B3",searchList_titulo: "Cesados", searchList_arrayData: self.filtro3))
        
        self.arrayFiltroFinal = arrayFiltro
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .plain, target: self, action:  #selector(btnUpdate(_:)))
        
        // Do any additional setup after loading the view.
    }

    @objc func btnUpdate(_ sender: UIButton!) {
        
        var arrayIndices = [IndexPath]()
        
        for index in self.arrayFiltroFinal.indices {
        let indexPath = IndexPath(row: index, section: 0)
        arrayIndices.append(indexPath)
        
        
        self.showIndexPath = !self.showIndexPath
        let animationStyle = self.showIndexPath ? UITableView.RowAnimation.right : .top
    
        tableView.reloadRows(at: arrayIndices, with: animationStyle)
        
        }
    }

}
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayFiltroFinal.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if !arrayFiltroFinal[section].estado{
            return 0
        }

        return self.arrayFiltroFinal[section].searchList_arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell  = tableView.dequeueReusableCell(withIdentifier: "ViewControllerTableViewCell", for: indexPath) as! ViewControllerTableViewCell
        
        cell.objColegio = self.arrayFiltroFinal[indexPath.section].searchList_arrayData[indexPath.row] as? Colegio
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view  = UIView()
        view.backgroundColor = .white
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.colorFromHexString("E5E5E5", withAlpha: 1).cgColor
        
        let lblTexto = UILabel()
        lblTexto.frame = CGRect(x: 35, y: 18, width: 250, height: 19)
        lblTexto.numberOfLines = 0
        lblTexto.text = "\(self.arrayFiltroFinal[section].searchList_titulo)"
        lblTexto.font = UIFont(name: "Roboto-Medium", size: 16)
        lblTexto.textColor = UIColor.colorFromHexString("676767", withAlpha: 1)
        
        let imagenView = UIView()
        imagenView.frame = CGRect(origin: CGPoint(x: 18, y: 25), size: CGSize(width: 7, height: 7))
        imagenView.backgroundColor = UIColor.colorFromHexString(self.arrayFiltroFinal[section].color, withAlpha: 1)
        imagenView.layer.cornerRadius = 3.5
        
        let button = UIButton(type: .system)
        button.setTitle("\(self.arrayFiltroFinal[section].searchList_titulo)", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.addTarget(self, action: #selector(self.btnAnimation), for: .touchUpInside)
        button.tag = section
//        view.addSubview(lblTexto)
//        view.addSubview(imagenView)
//        view.addSubview(button)
        return button
    }
    
    @objc func btnAnimation(_ sender: UIButton!){
    
        let section = sender.tag
        var arrayIndices = [IndexPath]()
        
        for row in self.arrayFiltroFinal[section].searchList_arrayData.indices{
            print(row)
            let index = IndexPath(row: row, section: section)
            arrayIndices.append(index)
        }
        
        let isExpandible = self.arrayFiltroFinal[section].estado
        self.arrayFiltroFinal[section].estado = !isExpandible
        
        if isExpandible{
            tableView.deleteRows(at: arrayIndices, with: .fade)
        }else{
            tableView.insertRows(at: arrayIndices, with: .left)
        }

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let botonDelete = UIContextualAction(style: .normal, title: "ELiminar") { (action, view, nil) in
            
        }
        
        let config = UISwipeActionsConfiguration(actions: [botonDelete])
        
        config.performsFirstActionWithFullSwipe = false
        
        return config
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return self.array.count
    }

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.array
    }
}
