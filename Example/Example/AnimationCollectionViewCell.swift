//
//  AnimationCollectionViewCell.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 3/17/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class AnimationCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblNombre : UILabel!
    @IBOutlet weak var lblApellido : UILabel!
    
    var objPrueba : Prueba!{
        didSet{
            self.lblNombre.text! = self.objPrueba.nombre
//            self.lblApellido.text! = self.objPrueba.Apellido
        }
    }
}
