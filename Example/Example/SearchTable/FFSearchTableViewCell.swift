//
//  FFSearchTableViewCell.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 5/31/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class FFSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var lblbNombre : UILabel!
    
    var obj : Search!{
        didSet{
            self.lblbNombre.text = self.obj.nombres
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
