//
//  FFOneViewController.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 5/31/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

protocol FFOneViewControllerDelegate {
    func getArray(_ controller : FFOneViewController) -> [FFSearchBE]
    func tableViewSearch(_ controller : FFOneViewController) -> UITableView?
}

class FFOneViewController: UIViewController {

      @IBOutlet weak var searchNombres : UISearchBar!
    var arraySearch = [FFSearchBE]()
    var arrayFiltro = [FFSearchBE]()
    var filtroDatos = [Search]()
    var filter      = [Search]()
    var delegate : FFOneViewControllerDelegate?
    
    @IBOutlet weak var tblSearch : UITableView!
    
    var state = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.arraySearch = self.delegate?.getArray(self) ?? [FFSearchBE]()
        self.load()
        self.tblSearch.reloadData()
        
//        self.arrayFiltro = datos.arrayFiltro
//        self.tblSearch = datos.tblSearch
        // Do any additional setup after loading the view.
    }
    
    func load() {
        self.arraySearch = [FFSearchBE(title: "A", array: [Search(nombres: "Alexander")]),FFSearchBE(title: "B", array: [Search(nombres: "Brenda")]), FFSearchBE(title: "C", array: [Search(nombres: "Cstillo")])]
        //    var arrayFiltro = [Search(nombres: "Alexander"),Search(nombres: "Brenda"),Search(nombres: "Catillo")]
            self.arrayFiltro = self.arraySearch
        self.filtroDatos = [Search(nombres: "Alexander"), Search(nombres: "Brenda"), Search(nombres: "Castillo"),Search(nombres: "Jorge"), Search(nombres: "Jorge"), Search(nombres: "Castillo"),Search(nombres: "Mika"), Search(nombres: "Brenda"), Search(nombres: "Brenda")]
        self.filter = self.filtroDatos
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
  
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
    }

    

}
extension FFOneViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.state == false{
            return self.arraySearch.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.state == false{
            return self.arraySearch[section].array.count
        }else{
           return self.filtroDatos.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if self.state == false{
            return self.tableViewNew(tableView, cellForRowAt: indexPath)
         }else{
            return self.tableViewSearch(tableView, cellForRowAt: indexPath)
        }
    }
    
    func tableViewNew(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FFSearchTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FFSearchTableViewCell
        cell.obj = self.arraySearch[indexPath.section].array[indexPath.row]
        return cell
    }
    
    func tableViewSearch(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FFSearchTableViewCell1"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FFSearchTableViewCell
        cell.obj = self.filtroDatos[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.state == false{
            return self.arraySearch[section].title
        }else{
            return ""
        }
        
    }
}

extension FFOneViewController : UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
//            self.filtroDatos = self.filtroDatos
            self.state = false
            self.tblSearch.reloadData()
//            self.filtroDatos = self.filter
        }else{
            self.filtroDatos = self.filter.filter({ (search) -> Bool in
                return "\(search.nombres)".contains(searchText)
            })
            self.state = true
            self.tblSearch.reloadData()
        }
    }
    
}
