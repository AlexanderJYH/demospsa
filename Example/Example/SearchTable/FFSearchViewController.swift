//
//  FFSearchViewController.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 5/31/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

struct FFSearchBE {
    
    var title = ""
    var array = [Search]()
}

struct Search {
    var nombres = ""
}

class FFSearchViewController: UIViewController, FFOneViewControllerDelegate {
    
    func getArray(_ controller: FFOneViewController) -> [FFSearchBE] {
        self.arraySearch
    }
    
    func tableViewSearch(_ controller: FFOneViewController) -> UITableView? {
        self.tblSearch
    }
    

    @IBOutlet weak var searchNombres : UISearchBar!
    @IBOutlet weak var tblSearch : UITableView!
    @IBOutlet weak var viewOne : UIView!
    @IBOutlet weak var viewTwo : UIView!
    
    
    var arraySearch = [FFSearchBE]()
    var arrayFiltro = [FFSearchBE]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.load()
        // Do any additional setup after loading the view.
    }
    
    func load() {
        self.arraySearch = [FFSearchBE(title: "A", array: [Search(nombres: "Alexander")]),FFSearchBE(title: "B", array: [Search(nombres: "Brenda")]), FFSearchBE(title: "C", array: [Search(nombres: "Cstillo")])]
        //    var arrayFiltro = [Search(nombres: "Alexander"),Search(nombres: "Brenda"),Search(nombres: "Catillo")]
            self.arrayFiltro = self.arraySearch
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "FFOneViewController"{
            let controller = segue.destination as! FFOneViewController
            controller.delegate = self
        }
    }
    

}

extension FFSearchViewController : UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            self.viewOne.alpha = 0
            self.viewTwo.alpha = 1
        }else{
            self.viewTwo.alpha = 0
            self.viewOne.alpha = 1
//            self.arraySearch = self.arrayFiltro.filter({ (search) -> Bool in
//                return "\(search.title)".contains(searchText.uppercased())
//            })
//            self.delegate?.tableViewSearch(self)?.reloadData()
        }
    }
    
}
