//
//  AgendaTableViewCell.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 3/25/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class AgendaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNombre : UILabel!

    var objAgenda : FFProspectoBE!{
        didSet{
            self.lblNombre.text! = self.objAgenda.apellido
//                self.objAgenda.nombre + " " + self.objAgenda.apellido
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
