//
//  ContactosViewController.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 3/13/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

struct ContactosBE {
    var title = ""
    var arrayVacio = [Prospecto]()
}

struct Prospecto {
    var appellido = ""
}

class ContactosViewController: UIViewController {
    
    @IBOutlet weak var tblContactos : UITableView!
    
    var arrayContactos = [ContactosBE]()
    var sectionTitles: [String] = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ#".map{ String($0)}
    
    var apellidos = ["Ynoñan","RAmires","Dolores","Alvares","Soli","Bolar","Zamora","Hurtado","Alvares", "Alvares","Bolar","Bolo"]
    
    var prospecto = [Prospecto(appellido: "Ynoñan"),Prospecto(appellido: "RAmires"),Prospecto(appellido: "Dolores"),Prospecto(appellido: "Alvares"),Prospecto(appellido: "Soli"),Prospecto(appellido: "Alres"),Prospecto(appellido: "Soli"),Prospecto(appellido: "Soli"),Prospecto(appellido: "Bolar"),Prospecto(appellido: "Zamora"),Prospecto(appellido: "Zoto"),Prospecto(appellido: "Vernal"),Prospecto(appellido: "Hurtado")]
    
    var apellidosDic = [String : [String]]()
    var arraydosDic = [String : [String]]()
    var arrayProspectoDic = [ContactosBE]()
    
    var apellidoSectionTitle = [String]()
    var contactosSectionTitle = [ContactosBE]()
//    var arrayApellidos = [Personas(appellido:"Ynoñan"), Personas(appellido:"Durand"), Personas(appellido:"Vizcarra"), Personas(appellido:"Lopez"),Personas(appellido: "Perez"),Personas(appellido: "Alvares")]
    
    var arrayLleno = [Prospecto]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor.white
        appearance.titleTextAttributes = [.foregroundColor: UIColor.lightText] // With a red background, make the title more readable.
        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
        navigationItem.compactAppearance = appearance // For iPhone small navigation bar in landscape.
//        self.navigationController?.navigationBar.largeTitleTextAttributes
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController!.navigationBar.barStyle = .default
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue,
                                                                             NSAttributedString.Key.font: UIFont(name: "Papyrus", size: 30) ??
                                    UIFont.systemFont(ofSize: 30)]
//        self.tblContactos.reloadData()
//        self.arrayContactos = [ContactosBE(title: "A", arrayVacio: [Personas(appellido: "Alvares")]),ContactosBE(title: "B", arrayVacio: [Personas(appellido: "Bernal")]),ContactosBE(title: "C", arrayVacio: [Personas(appellido: "Carlvajal")]),ContactosBE(title: "D", arrayVacio: [Personas(appellido: "Durand")]),ContactosBE(title: "E", arrayVacio: [Personas(appellido: "Elerna")]),ContactosBE(title: "F", arrayVacio: [Personas(appellido: "Farias")]),ContactosBE(title: "G", arrayVacio: [Personas(appellido: "Ginles")]),ContactosBE(title: "H", arrayVacio: [Personas(appellido: "Hidalgo")]),ContactosBE(title: "I", arrayVacio: [Personas(appellido: "Idalgo"),Personas(appellido: "Idalgo"),Personas(appellido: "Idalgo"),Personas(appellido: "Idalgo"),Personas(appellido: "Idalgo")]),ContactosBE(title: "J", arrayVacio: [Personas(appellido: "Julas")]),ContactosBE(title: "K", arrayVacio: [Personas(appellido: "Karin")]),ContactosBE(title: "L", arrayVacio: [Personas(appellido: "Lopes")]),ContactosBE(title: "M", arrayVacio: [Personas(appellido: "Morales")]),ContactosBE(title: "N", arrayVacio: [Personas(appellido: "Norales")]),ContactosBE(title: "", arrayVacio: [Personas(appellido: "Norales")]),ContactosBE(title: "N", arrayVacio: [Personas(appellido: "Norales")]),ContactosBE(title: "Ñ", arrayVacio: [Personas(appellido: "Nonan")]),ContactosBE(title: "O", arrayVacio: [Personas(appellido: "Olares")]),ContactosBE(title: "P", arrayVacio: [Personas(appellido: "Peres")]),ContactosBE(title: "Q", arrayVacio: [Personas(appellido: "Qualer")]),ContactosBE(title: "R", arrayVacio: [Personas(appellido: "Rosa")]),ContactosBE(title: "S", arrayVacio: [Personas(appellido: "Santos")]),ContactosBE(title: "T", arrayVacio: [Personas(appellido: "Torres")]),ContactosBE(title: "U", arrayVacio: [Personas(appellido: "Ulares")]),ContactosBE(title: "V", arrayVacio: [Personas(appellido: "Vares")]),ContactosBE(title: "W", arrayVacio: [Personas(appellido: "Waldo")]),ContactosBE(title: "X", arrayVacio: [Personas(appellido: "Xales")]),ContactosBE(title: "Y", arrayVacio: [Personas(appellido: "Ynoñan"),Personas(appellido: "Ysolt"),Personas(appellido: "Ynas"),Personas(appellido: "Yhabl"),Personas(appellido: "Yhabl"),Personas(appellido: "Yhabl"),Personas(appellido: "Yhabl"),Personas(appellido: "Yhabl"),Personas(appellido: "Yhabl"),Personas(appellido: "Yhabl")]),ContactosBE(title: "Z", arrayVacio: [Personas(appellido: "Zamora"),Personas(appellido: "Zolis"),Personas(appellido: "Zoto"),Personas(appellido: "Zolar"),Personas(appellido: "Zali"),Personas(appellido: "Zolar"),Personas(appellido: "Zali"),Personas(appellido: "Zanter"),Personas(appellido: "Zorro")])]
        // Do any additional setup after loading the view.
        self.datos()
    }

    
    
    func datos(){
        
        for obj in apellidos{
            
            let firsIndex = obj.index(obj.startIndex, offsetBy: 1)
            let apellidosKey = String(obj[..<firsIndex])
         
            if var apellidoValues = apellidosDic[apellidosKey]{
                apellidoValues.append(obj)
                apellidosDic[apellidosKey] = apellidoValues
            }else{
                apellidosDic[apellidosKey] = [obj]

                
            }

            apellidoSectionTitle = [String](apellidosDic.keys)
            apellidoSectionTitle = apellidoSectionTitle.sorted(by: {$0 < $1})
        }
        print(apellidosDic)
        
        
//        for state in sortedStates{
//            guard let first = state.name.first, let sectionIndex = sectionTitles.firstIndex(of: String(first)) else { return }
//            sections[sectionIndex].append(state)
//
//        }
        
        for objProspecto in prospecto{
            let indexFirst = objProspecto.appellido.index(objProspecto.appellido.startIndex , offsetBy: 1)
            let prospecKey = objProspecto.appellido[..<indexFirst]
            
            
            

//            self.arrayProspectoDic.append(ContactosBE(title: "\(indexFirst)", arrayVacio: [objProspecto]))
        }

        print(arrayProspectoDic)
        
    }
    

}

extension ContactosViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayProspectoDic.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayProspectoDic[section].arrayVacio.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactosTableViewCell", for: indexPath) as! ContactosTableViewCell
        
        cell.objContactos = self.arrayProspectoDic[indexPath.section].arrayVacio[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        30
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.arrayProspectoDic[section].title
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.sectionTitles
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        
        return index
    }
}
