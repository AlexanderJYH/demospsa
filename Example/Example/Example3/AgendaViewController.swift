//
//  AgendaViewController.swift
//  Example
//
//  Created by Alexander Johel Ynoñan H on 3/25/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

struct State {
    var name = ""
}

struct Letras {
    var name = ""
    var array = [String]()
}

struct FFLetrasBE {
    var letra = ""
    var arrayProspect = [FFProspectoBE]()
}

struct FFProspectoBE {
    var nombre = ""
    var apellido = ""
    var id = 0
}

struct FFLLenoBE {
    var arrayProspect = [FFProspectoBE]()
    var arryLetr = [FFLetrasBE]()
}

struct FFDatosBE {
    var letras : String
    var array : [FFProspectoBE]
}

class AgendaViewController: UIViewController {
    
    @IBOutlet weak var tblAgenda : UITableView!
    
    let allStates = ["Alaska",
    "Alabama",
    "Alabama",
    "Arkansas",
    "American Samoa",
    "Arizona",
    "California",
    "Colorado",
    "Connecticut",
    "District of Columbia",
    "Delaware",
    "Florida",
    "Georgia",
    "Guam",
    "Virginia",
    "Virgin Islands",
    "Vermont",
    "Washington",
    "Wisconsin",
    "West Virginia",
    "Wyoming"]
    
    
    var arrayFinal = [FFProspectoBE(nombre: "Alexander", apellido: "Ynoñan Rosas", id: 10),FFProspectoBE(nombre: "Brenda", apellido: "Ynoñan Huratado", id: 10),FFProspectoBE(nombre: "Roxana", apellido: "Huyllapuma", id: 10),FFProspectoBE(nombre: "Rosio", apellido: "Durand", id: 10),FFProspectoBE(nombre: "Alvaro", apellido: "Alvarader", id: 10),FFProspectoBE(nombre: "Tal", apellido: "Torres", id: 10),FFProspectoBE(nombre: "Xio", apellido: "Linares", id: 10),FFProspectoBE(nombre: "Carlos", apellido: "Canto", id: 10),FFProspectoBE(nombre: "Samir", apellido: "Peres", id: 10),FFProspectoBE(nombre: "Son", apellido: "Mayor", id: 10),FFProspectoBE(nombre: "Son", apellido: "Mayor", id: 10),FFProspectoBE(nombre: "Son", apellido: "Altar", id: 10)]
    var arrayConTodo = [FFLetrasBE]()
    
    var arrayLetrasFinal = [FFLetrasBE]()
    var styleTable = false
    let collation = UILocalizedIndexedCollation.current()
    var sectionTitles: [String] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#".map { String($0)}
    var sections: [[State]] = []
    var datosBE = [FFDatosBE]()
    var arrayFinalFor = [FFLetrasBE]()
    
    @objc func loadStates() {
        self.refreshControl.beginRefreshing()
        self.arrayFinalFor.removeAll()
        
        
        var stringAny = [String]()
        for obj in self.arrayFinal{
           
            let letra = obj.apellido.prefix(1)
            stringAny.append(String(letra))
        }

        let namesStartingWithM = stringAny.removeDuplicates().sorted()
        
        for obj in namesStartingWithM{
            let datos = namesStartingWithM.map({FFLetrasBE(letra: $0, arrayProspect: self.arrayFinal.filter({$0.apellido.prefix(1) == obj}))})
            self.arrayFinalFor.append(FFLetrasBE(letra: obj, arrayProspect: datos.first!.arrayProspect))
        }
        
        print(namesStartingWithM)
        
        print("**********________________***********")
            
            
        let groupedDictionary = Dictionary(grouping: self.allStates, by: {String($0.prefix(1))})
        let keys = groupedDictionary.keys.sorted()
        let lana = keys.map({ Letras(name: $0, array: (groupedDictionary[$0]?.sorted())!) })
        self.arrayDatos = lana
        self.refreshControl.endRefreshing()
        self.tblAgenda.reloadData()

            
    }
    
    var arrayDatos = [Letras](){
        didSet{
            
        }
    }
    
    lazy var refreshControl : UIRefreshControl = {
        
        var _refreshControl = UIRefreshControl()
        _refreshControl.tintColor = .gray
        _refreshControl.addTarget(self, action: #selector(self.loadStates), for: .valueChanged)
        _refreshControl.attributedTitle = NSAttributedString(string: "Porfavor espere ...")
        return _refreshControl
    }()
    
    
    var datos2 = [String]()
    var lado = [String]()
    var sections1 : [[Letras]] = []
    var general = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblAgenda.addSubview(self.refreshControl)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadStates()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HaptiTouchViewController"{
            let controller = segue.destination as! HaptiTouchViewController
            controller.objProspecto = sender as? FFProspectoBE
        }
    }
    var selectedIndexPath : IndexPath?
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if traitCollection.forceTouchCapability == UIForceTouchCapability.available {
            
        } else {
            print("No es compatible")
        }
    }
    
}

extension AgendaViewController : UIViewControllerPreviewingDelegate{

    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        self.selectedIndexPath = self.tblAgenda.indexPathForRow(at: location)
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let prevista = storyboard.instantiateViewController(withIdentifier: "PopIntermedioViewController") as! PopIntermedioViewController
//        prevista.objProspecto = self.arrayFin alFor[self.selectedIndexPath!.section].arrayProspect[selectedIndexPath?.row ?? 0]
        return prevista
    }

    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {

        self.selectedIndexPath = self.tblAgenda.indexPathForSelectedRow

        var datos = self.arrayFinalFor[self.selectedIndexPath!.section].arrayProspect[selectedIndexPath?.row ?? 0]
        
        self.performSegue(withIdentifier: "PopIntermedioViewController", sender:  datos )
        
     
    }


}


extension AgendaViewController : UITableViewDelegate, UITableViewDataSource{
//
//    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, willPerformPreviewActionForMenuWith configuration: UIContextMenuConfiguration, animator: UIContextMenuInteractionCommitAnimating) {
//
//        // If we used a view controller for our preview, we can pull it out of the animator and show it once the commit animation is complete.
//        animator.addCompletion {
//            self.performSegue(withIdentifier: "HaptiTouchViewController", sender: nil)
//        }
//    }
//    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
//
//        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { suggestedActions in
//
//            let obj = self.arrayFinalFor[indexPath.section].arrayProspect[indexPath.row]
//
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let prevista = storyboard.instantiateViewController(withIdentifier: "PopIntermedioViewController") as! PopIntermedioViewController
//            prevista.objProspecto = obj
//
//
//
//            let share = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { action in
//                self.performSegue(withIdentifier: "HaptiTouchViewController", sender: obj)
//            }
//
//            let taper = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { action in
//
//                // Show system share sheet
//            }
//
//            // Create an action for renaming
//            let rename = UIAction(title: "Rename", image: UIImage(systemName: "square.and.pencil")) { action in
//                // Perform renaming
//            }
//
//            // Here we specify the "destructive" attribute to show that it’s destructive in nature
//            let delete = UIAction(title: "Delete", image: UIImage(systemName: "trash"), attributes: .destructive) { action in
//                // Perform delete
//            }
//
//            // Create and return a UIMenu with all of the actions as children
//            return UIMenu(title: "", children: [share, taper, rename])
//        }
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayFinalFor.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayFinalFor[section].arrayProspect.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AgendaTableViewCell", for: indexPath) as! AgendaTableViewCell
        let section = arrayFinalFor[indexPath.section]
        let username = section.arrayProspect[indexPath.row]
        cell.objAgenda = username
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.arrayFinalFor[indexPath.section].arrayProspect[indexPath.row]
        let position = indexPath.row
        self.performSegue(withIdentifier: "HaptiTouchViewController", sender: obj)
        print("\(obj), \(position)")
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let obj = self.arrayFinalFor[indexPath.section].arrayProspect[indexPath.row]
        
        let botonDelete = UIContextualAction(style: .normal, title: "jk") { (action, view, nil) in
            
            self.arrayFinalFor[indexPath.section].arrayProspect.remove(at: indexPath.row)
            
            print("\(obj) - \(self.arrayFinalFor)")
//            self.tblAgenda.reloadData()
        }

        botonDelete.image = UIImage(named: "deleteICon")
        botonDelete.backgroundColor = UIColor.colorFromHexString("#E14941", withAlpha: 1)
// MARK: Swipe Enable
        let config = UISwipeActionsConfiguration(actions: [botonDelete])
        config.performsFirstActionWithFullSwipe = false
        
        return config
    }
     
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.arrayFinalFor[section].letra
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.arrayFinalFor.map({$0.letra})
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
}

extension Array
{
    func filterDuplicate(_ keyValue:((AnyHashable...)->AnyHashable,Element)->AnyHashable) -> [Element]
    {
        func makeHash(_ params:AnyHashable ...) -> AnyHashable
        {
           var hash = Hasher()
           params.forEach{ hash.combine($0) }
           return hash.finalize()
        }
        var uniqueKeys = Set<AnyHashable>()
        return filter{uniqueKeys.insert(keyValue(makeHash,$0)).inserted}
    }
}
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
