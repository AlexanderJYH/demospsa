//
//  TabOneTableCollectionViewCell.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 6/14/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class TabOneTableCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewSelect : UIView!
    @IBOutlet weak var lblTitle : UILabel!
    
    var objTabla = EvaluacinesBE()
    
    var objQuestion : QuestionBE!{
        didSet{
            self.lblTitle.text = "\(self.objQuestion.numero)"
            
        }
    }
    
    func validarCeldaColor( estado : Bool){
            
        if estado{
            self.viewSelect.backgroundColor = .red
        }else{
            self.viewSelect.backgroundColor = .white
        }
    }
    
}
