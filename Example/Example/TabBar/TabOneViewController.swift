//
//  TabOneViewController.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 6/11/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

protocol TabOneViewControllerDelegate {
    func pruebaExit( controller : TabOneViewController, estado : Bool?)
}

struct EvaluacinesBE {
    var stateTable = false
    var title = ""
    var arrayQuestion = [QuestionBE]()
}

struct QuestionBE {
    var numero = 0
    var selectNumber = false
}

class TabOneViewController: UIViewController {

    var  arrayEvalua = [EvaluacinesBE(title: "ALexander",arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]), EvaluacinesBE(title: "Roxana", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Brenda", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Maria", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Alicia", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Diego", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Pedro", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Elisban", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Vero", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Juan", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Alberto", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Erick", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)]),EvaluacinesBE(title: "Rafael", arrayQuestion: [QuestionBE(numero: 1),QuestionBE(numero: 2),QuestionBE(numero: 3),QuestionBE(numero: 4)])]
    
    var delegate : TabOneViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnExit(_ sender : UIButton!){
        self.performSegue(withIdentifier: "TabExitViewController", sender: nil)
    }
    
    @IBAction func btnExitTAB(_ sender : UIButton!){
        self.dismiss(animated: true) {
            self.delegate?.pruebaExit(controller: self, estado: true)
        }
//        self.navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TabOneViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayEvalua.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "TabOneTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! TabOneTableViewCell
        cell.objEvaluaciones = self.arrayEvalua[indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension TabOneViewController : TabOneTableViewCellDelegate{
    
    func TabOne(_ controller: TabOneTableViewCell, didSelect: FFAlumnos) {
        print(didSelect)
    }
    
    func TabOne(_ controller: TabOneTableViewCell, didDeselect: FFAlumnos) {
        
    }
}
