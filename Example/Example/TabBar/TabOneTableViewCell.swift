//
//  TabOneTableViewCell.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 6/14/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

protocol TabOneTableViewCellDelegate {
    func TabOne(_ controller : TabOneTableViewCell, didSelect : FFAlumnos)
    func TabOne(_ controller : TabOneTableViewCell, didDeselect : FFAlumnos)
}

class TabOneTableViewCell : UITableViewCell {

    @IBOutlet weak var clvTab : UICollectionView!
    @IBOutlet weak var lblTitle : UILabel!
 
    var delegate : TabOneTableViewCellDelegate?
    
    var selecion = false
    
    var objEvaluaciones : EvaluacinesBE!{
        didSet{
            self.lblTitle.text = self.objEvaluaciones.title
            
        }
    }
    
    
    override func draw(_ rect: CGRect) {
        self.clvTab.delegate = self
        self.clvTab.dataSource = self
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension TabOneTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.objEvaluaciones.arrayQuestion.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "TabOneTableCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! TabOneTableCollectionViewCell
        cell.objTabla = self.objEvaluaciones
        cell.objQuestion = self.objEvaluaciones.arrayQuestion[indexPath.row]
        cell.validarCeldaColor(estado: self.objEvaluaciones.arrayQuestion[indexPath.row].selectNumber)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selecion = true
        self.objEvaluaciones.arrayQuestion[indexPath.row].selectNumber = self.selecion
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
   }
    
    
}

