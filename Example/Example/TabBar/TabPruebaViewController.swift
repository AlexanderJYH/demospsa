//
//  TabPruebaViewController.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 6/11/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

class TabPruebaViewController: UIViewController ,TabOneViewControllerDelegate{
    
    func pruebaExit(controller: TabOneViewController, estado: Bool?) {
        if estado == true{
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnTab(_ sender : UIButton!){
        self.performSegue(withIdentifier: "TabOne", sender: nil)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "TabOne"{
            let controller = segue.destination as? TabOneViewController
            controller?.delegate = self
        }
    }

    
    

}
