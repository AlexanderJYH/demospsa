//
//  FFFormularioTableViewCell.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 5/27/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

protocol FFFormularioTableViewCellDelegate {
    
    func deleteButton(controller : FFFormularioTableViewCell,_ sender : UIButton)
    func getFormAlumno(controller : FFFormularioTableViewCell,_ sender : FFAlumnos)
}


class FFFormularioTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var txtNombre : UITextField!
    @IBOutlet weak var txtApellido : UITextField!
    @IBOutlet weak var btnStyleDelete : UIButton!
    
    var delegate : FFFormularioTableViewCellDelegate?
    var objAlumnos = FFAlumnos()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtNombre.delegate = self
        self.txtApellido.delegate = self
        
        self.txtApellido.text = ""
        self.txtApellido.text = ""
        
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: Cundo se cierre el teclado se ejecuta este metodo
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == self.txtNombre {
            self.objAlumnos.nombres = self.txtNombre.text ?? ""
        }else if textField == self.txtApellido{
            self.objAlumnos.apellido = self.txtApellido.text ?? ""
        }else{
            print("Ingrese Datos")
        }
        
        self.delegate?.getFormAlumno(controller: self, self.objAlumnos)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
         if textField == self.txtNombre {
             self.objAlumnos.nombres = ""
         }else if textField == self.txtApellido{
             self.objAlumnos.apellido = ""
         }else{
             print("Ingrese Datos")
         }
    }

    @IBAction func btnDelete(_ sender : UIButton!){
        self.delegate?.deleteButton(controller: self, sender)
    }
    
}
