//
//  FFFormularioViewController.swift
//  Example
//
//  Created by Alexander Ynoñan H. on 5/27/20.
//  Copyright © 2020 Alexander Johel Ynoñan H. All rights reserved.
//

import UIKit

struct FFAlumnos {
    var nombres = ""
    var apellido = ""
}

class FFFormularioViewController: UIViewController {
    
    @IBOutlet weak var txtNombre : UITextField!
    @IBOutlet weak var txtApellido : UITextField!
    @IBOutlet weak var txtDni : UITextField!
    @IBOutlet weak var tblFormAlumno : UITableView!
    
    
    var arrayAlumnos = [String]()

    var getAlumnos  = FFAlumnos()
    var objVacio    = FFAlumnos()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnNext(_ sender : UIButton!){
        
        if self.txtNombre.text == ""{
            return self.showAltert(withTitle: "Error", withMessage: "Nombre", withAcceptButton: "ACeptar", withCompletion: nil)
        }
        
        if self.txtApellido.text == ""{
            return self.showAltert(withTitle: "Error", withMessage: "Apellido", withAcceptButton: "ACeptar", withCompletion: nil)
        }
        
        if self.txtDni.text == ""{
            return self.showAltert(withTitle: "Error", withMessage: "dni", withAcceptButton: "ACeptar", withCompletion: nil)
        }
        
        print("Paso La vista")
        
    }

    @IBAction func btnAddForm(_ sender : UIButton!){
        
        if self.txtNombre.text == ""{
            return self.showAltert(withTitle: "Error", withMessage: "Nombre", withAcceptButton: "ACeptar", withCompletion: nil)
        }
        
        if self.txtApellido.text == ""{
            return self.showAltert(withTitle: "Error", withMessage: "Apellido", withAcceptButton: "ACeptar", withCompletion: nil)
        }
        
        if self.txtDni.text == ""{
            return self.showAltert(withTitle: "Error", withMessage: "dni", withAcceptButton: "ACeptar", withCompletion: nil)
        }
        
        if self.arrayAlumnos.count == 0{
            self.arrayAlumnos.append("uno")
            self.tblFormAlumno.reloadData()
        }else{
        
            
            if self.getAlumnos.nombres == ""{
                return self.showAltert(withTitle: "Error", withMessage: "Nombre", withAcceptButton: "ACeptar", withCompletion: nil)
            }
        
            if self.getAlumnos.apellido == ""{
                return self.showAltert(withTitle: "Error", withMessage: "apeliido", withAcceptButton: "ACeptar", withCompletion: nil)
            }
            
            self.arrayAlumnos.append("dos")
            self.tblFormAlumno.reloadData()
        
        }
        
    }
    
    @IBAction func tapCerrarTeclado(_ sender: AnyObject?) {
 
        self.view.endEditing(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FFFormularioViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayAlumnos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "FFFormularioTableViewCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FFFormularioTableViewCell
        cell.delegate = self
        cell.btnStyleDelete.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
    
    
}

extension FFFormularioViewController : FFFormularioTableViewCellDelegate{
    
    func deleteButton(controller: FFFormularioTableViewCell, _ sender: UIButton) {
        self.arrayAlumnos.remove(at: sender.tag)
        self.tblFormAlumno.reloadData()
        
    }
    
    func getFormAlumno(controller: FFFormularioTableViewCell, _ sender: FFAlumnos) {
        self.getAlumnos = sender
        print(self.getAlumnos.nombres,self.getAlumnos.apellido)
    }
    
}
